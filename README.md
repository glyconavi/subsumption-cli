# subsumption-cli

## build

```
(base) yamada@IY-MBPT16 subsumption-cli % gradle build                                                            

Deprecated Gradle features were used in this build, making it incompatible with Gradle 7.0.
Use '--warning-mode all' to show the individual deprecation warnings.
See https://docs.gradle.org/6.8.3/userguide/command_line_interface.html#sec:command_line_warnings

BUILD SUCCESSFUL in 1s
9 actionable tasks: 9 executed
```
## Usage 1

`./run.sh {input wurcs file} > {output ntriples}`

```
% ./run.sh test-wurcs.txt > sub.nt             
```

### ntriples to turtle

```
% rapper -i ntriples -o turtle sub.nt > sub.ttl
```


## Usage 2

```
 % java -jar build/libs/subsumption-cli-all.jar "WURCS=2.0/3,11,10/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3-3-3-3-3-3-3/a4-b1_b4-c1_c3-d1_c6-g1_d2-e1_e2-f1_g3-h1_g6-j1_h2-i1_j2-k1" | grep -v DEBUG 
<http://rdf.glycoinfo.org/glycan/f2847529a5bc608820eae5950f9f92ae8fe2bb9c99e3ecd83d0a275dc72de9b2>	<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>	<http://www.glycoinfo.org/glyco/owl/relation#Linkage_defined_saccharide> .
<http://rdf.glycoinfo.org/glycan/f2847529a5bc608820eae5950f9f92ae8fe2bb9c99e3ecd83d0a275dc72de9b2>	<http://www.w3.org/1999/02/22-rdf-syntax-ns#label>	"WURCS=2.0/3,11,10/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3-3-3-3-3-3-3/a4-b1_b4-c1_c3-d1_c6-g1_d2-e1_e2-f1_g3-h1_g6-j1_h2-i1_j2-k1" .
<http://rdf.glycoinfo.org/glycan/f2847529a5bc608820eae5950f9f92ae8fe2bb9c99e3ecd83d0a275dc72de9b2>	<http://www.glycoinfo.org/glyco/owl/relation#has_topology>	<http://rdf.glycoinfo.org/glycan/f7413afd778186571ec5f613feedb4b285821c3a673be85de83c31195b8807d8> .
<http://rdf.glycoinfo.org/glycan/f7413afd778186571ec5f613feedb4b285821c3a673be85de83c31195b8807d8>	<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>	<http://www.glycoinfo.org/glyco/owl/relation#Glycosidic_topology> .
<http://rdf.glycoinfo.org/glycan/f7413afd778186571ec5f613feedb4b285821c3a673be85de83c31195b8807d8>	<http://www.w3.org/1999/02/22-rdf-syntax-ns#label>	"WURCS=2.0/2,11,10/[a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5]/1-1-2-2-2-2-2-2-2-2-2/a?-b1_b?-c1_c?-d1_c?-i1_d?-e1_d?-g1_e?-f1_g?-h1_i?-j1_j?-k1" .
<http://rdf.glycoinfo.org/glycan/f7413afd778186571ec5f613feedb4b285821c3a673be85de83c31195b8807d8>	<http://www.glycoinfo.org/glyco/owl/relation#subsumes>	<http://rdf.glycoinfo.org/glycan/f2847529a5bc608820eae5950f9f92ae8fe2bb9c99e3ecd83d0a275dc72de9b2> .
<http://rdf.glycoinfo.org/glycan/f7413afd778186571ec5f613feedb4b285821c3a673be85de83c31195b8807d8>	<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>	<http://www.glycoinfo.org/glyco/owl/relation#Glycosidic_topology> .
<http://rdf.glycoinfo.org/glycan/f7413afd778186571ec5f613feedb4b285821c3a673be85de83c31195b8807d8>	<http://www.w3.org/1999/02/22-rdf-syntax-ns#label>	"WURCS=2.0/2,11,10/[a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5]/1-1-2-2-2-2-2-2-2-2-2/a?-b1_b?-c1_c?-d1_c?-i1_d?-e1_d?-g1_e?-f1_g?-h1_i?-j1_j?-k1" .
<http://rdf.glycoinfo.org/glycan/f7413afd778186571ec5f613feedb4b285821c3a673be85de83c31195b8807d8>	<http://www.glycoinfo.org/glyco/owl/relation#has_composition_with_linkage>	<http://rdf.glycoinfo.org/glycan/1380535e19fab1400a20e427fbb40fc37b563fec1e440fabf81f657801a84517> .
<http://rdf.glycoinfo.org/glycan/1380535e19fab1400a20e427fbb40fc37b563fec1e440fabf81f657801a84517>	<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>	<http://www.glycoinfo.org/glyco/owl/relation#Monosaccharide_composition_with_linkage> .
<http://rdf.glycoinfo.org/glycan/1380535e19fab1400a20e427fbb40fc37b563fec1e440fabf81f657801a84517>	<http://www.w3.org/1999/02/22-rdf-syntax-ns#label>	"WURCS=2.0/2,11,10/[u2122h_2*NCC/3=O][u1122h]/1-1-2-2-2-2-2-2-2-2-2/a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?" .
<http://rdf.glycoinfo.org/glycan/1380535e19fab1400a20e427fbb40fc37b563fec1e440fabf81f657801a84517>	<http://www.glycoinfo.org/glyco/owl/relation#subsumes>	<http://rdf.glycoinfo.org/glycan/f7413afd778186571ec5f613feedb4b285821c3a673be85de83c31195b8807d8> .
<http://rdf.glycoinfo.org/glycan/1380535e19fab1400a20e427fbb40fc37b563fec1e440fabf81f657801a84517>	<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>	<http://www.glycoinfo.org/glyco/owl/relation#Monosaccharide_composition_with_linkage> .
<http://rdf.glycoinfo.org/glycan/1380535e19fab1400a20e427fbb40fc37b563fec1e440fabf81f657801a84517>	<http://www.w3.org/1999/02/22-rdf-syntax-ns#label>	"WURCS=2.0/2,11,10/[u2122h_2*NCC/3=O][u1122h]/1-1-2-2-2-2-2-2-2-2-2/a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?" .
<http://rdf.glycoinfo.org/glycan/1380535e19fab1400a20e427fbb40fc37b563fec1e440fabf81f657801a84517>	<http://www.glycoinfo.org/glyco/owl/relation#has_base_composition>	<http://rdf.glycoinfo.org/glycan/62b8df24fc9c43b8e58d2ccef86f346c975523b67e497928458de83dbddefc29> .
<http://rdf.glycoinfo.org/glycan/62b8df24fc9c43b8e58d2ccef86f346c975523b67e497928458de83dbddefc29>	<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>	<http://www.glycoinfo.org/glyco/owl/relation#Monosaccharide_composition> .
<http://rdf.glycoinfo.org/glycan/62b8df24fc9c43b8e58d2ccef86f346c975523b67e497928458de83dbddefc29>	<http://www.w3.org/1999/02/22-rdf-syntax-ns#label>	"WURCS=2.0/2,11,0+/[u2122h_2*NCC/3=O][u1122h]/1-1-2-2-2-2-2-2-2-2-2/" .
<http://rdf.glycoinfo.org/glycan/62b8df24fc9c43b8e58d2ccef86f346c975523b67e497928458de83dbddefc29>	<http://www.glycoinfo.org/glyco/owl/relation#subsumes>	<http://rdf.glycoinfo.org/glycan/1380535e19fab1400a20e427fbb40fc37b563fec1e440fabf81f657801a84517> .
<http://rdf.glycoinfo.org/glycan/1380535e19fab1400a20e427fbb40fc37b563fec1e440fabf81f657801a84517>	<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>	<http://www.glycoinfo.org/glyco/owl/relation#Monosaccharide_composition_with_linkage> .
<http://rdf.glycoinfo.org/glycan/1380535e19fab1400a20e427fbb40fc37b563fec1e440fabf81f657801a84517>	<http://www.w3.org/1999/02/22-rdf-syntax-ns#label>	"WURCS=2.0/2,11,10/[u2122h_2*NCC/3=O][u1122h]/1-1-2-2-2-2-2-2-2-2-2/a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?" .
<http://rdf.glycoinfo.org/glycan/1380535e19fab1400a20e427fbb40fc37b563fec1e440fabf81f657801a84517>	<http://www.glycoinfo.org/glyco/owl/relation#has_base_composition>	 <http://rdf.glycoinfo.org/glycan/dd16ebb7c0faf52646a87e4e67297b71299cc71b8a9b818fc46a0dd61c05c3f1>  .
<http://rdf.glycoinfo.org/glycan/dd16ebb7c0faf52646a87e4e67297b71299cc71b8a9b818fc46a0dd61c05c3f1>	<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>	<http://www.glycoinfo.org/glyco/owl/relation#Base_composition_with_linkage> .
<http://rdf.glycoinfo.org/glycan/dd16ebb7c0faf52646a87e4e67297b71299cc71b8a9b818fc46a0dd61c05c3f1>	<http://www.w3.org/1999/02/22-rdf-syntax-ns#label>	"WURCS=2.0/2,11,10/[uxxxxh_2*NCC/3=O][uxxxxh]/1-1-2-2-2-2-2-2-2-2-2/a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?" .
<http://rdf.glycoinfo.org/glycan/dd16ebb7c0faf52646a87e4e67297b71299cc71b8a9b818fc46a0dd61c05c3f1>	<http://www.glycoinfo.org/glyco/owl/relation#subsumes>	<http://rdf.glycoinfo.org/glycan/1380535e19fab1400a20e427fbb40fc37b563fec1e440fabf81f657801a84517> .
<http://rdf.glycoinfo.org/glycan/62b8df24fc9c43b8e58d2ccef86f346c975523b67e497928458de83dbddefc29>	<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>	<http://www.glycoinfo.org/glyco/owl/relation#Monosaccharide_composition> .
<http://rdf.glycoinfo.org/glycan/62b8df24fc9c43b8e58d2ccef86f346c975523b67e497928458de83dbddefc29>	<http://www.w3.org/1999/02/22-rdf-syntax-ns#label>	"WURCS=2.0/2,11,0+/[u2122h_2*NCC/3=O][u1122h]/1-1-2-2-2-2-2-2-2-2-2/" .
<http://rdf.glycoinfo.org/glycan/62b8df24fc9c43b8e58d2ccef86f346c975523b67e497928458de83dbddefc29>	<http://www.glycoinfo.org/glyco/owl/relation#has_composition>	<http://rdf.glycoinfo.org/glycan/9333d76e78db0cbba5f2c386d95271030d2d7a08d44f25966edef58810a9c6fb> .
<http://rdf.glycoinfo.org/glycan/9333d76e78db0cbba5f2c386d95271030d2d7a08d44f25966edef58810a9c6fb>	<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>	<http://www.glycoinfo.org/glyco/owl/relation#Base_composition> .
<http://rdf.glycoinfo.org/glycan/9333d76e78db0cbba5f2c386d95271030d2d7a08d44f25966edef58810a9c6fb>	<http://www.w3.org/1999/02/22-rdf-syntax-ns#label>	"WURCS=2.0/2,11,0+/[uxxxxh_2*NCC/3=O][uxxxxh]/1-1-2-2-2-2-2-2-2-2-2/" .
<http://rdf.glycoinfo.org/glycan/9333d76e78db0cbba5f2c386d95271030d2d7a08d44f25966edef58810a9c6fb>	<http://www.glycoinfo.org/glyco/owl/relation#subsumes>	<http://rdf.glycoinfo.org/glycan/62b8df24fc9c43b8e58d2ccef86f346c975523b67e497928458de83dbddefc29> .
<http://rdf.glycoinfo.org/glycan/dd16ebb7c0faf52646a87e4e67297b71299cc71b8a9b818fc46a0dd61c05c3f1>	<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>	<http://www.glycoinfo.org/glyco/owl/relation#Base_composition_with_linkage> .
<http://rdf.glycoinfo.org/glycan/dd16ebb7c0faf52646a87e4e67297b71299cc71b8a9b818fc46a0dd61c05c3f1>	<http://www.w3.org/1999/02/22-rdf-syntax-ns#label>	"WURCS=2.0/2,11,10/[uxxxxh_2*NCC/3=O][uxxxxh]/1-1-2-2-2-2-2-2-2-2-2/a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?}-{a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?" .
<http://rdf.glycoinfo.org/glycan/dd16ebb7c0faf52646a87e4e67297b71299cc71b8a9b818fc46a0dd61c05c3f1>	<http://www.glycoinfo.org/glyco/owl/relation#has_composition>	<http://rdf.glycoinfo.org/glycan/9333d76e78db0cbba5f2c386d95271030d2d7a08d44f25966edef58810a9c6fb> .
<http://rdf.glycoinfo.org/glycan/9333d76e78db0cbba5f2c386d95271030d2d7a08d44f25966edef58810a9c6fb>	<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>	<http://www.glycoinfo.org/glyco/owl/relation#Base_composition> .
<http://rdf.glycoinfo.org/glycan/9333d76e78db0cbba5f2c386d95271030d2d7a08d44f25966edef58810a9c6fb>	<http://www.w3.org/1999/02/22-rdf-syntax-ns#label>	"WURCS=2.0/2,11,0+/[uxxxxh_2*NCC/3=O][uxxxxh]/1-1-2-2-2-2-2-2-2-2-2/" .
<http://rdf.glycoinfo.org/glycan/9333d76e78db0cbba5f2c386d95271030d2d7a08d44f25966edef58810a9c6fb>	<http://www.glycoinfo.org/glyco/owl/relation#subsumes>	<http://rdf.glycoinfo.org/glycan/dd16ebb7c0faf52646a87e4e67297b71299cc71b8a9b818fc46a0dd61c05c3f1> .

```
