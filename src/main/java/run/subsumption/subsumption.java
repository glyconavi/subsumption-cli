package subsumption;

import java.io.UnsupportedEncodingException;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.security.MessageDigest;
import org.glycoinfo.subsumption.*;
import java.util.LinkedList;
import org.glycoinfo.WURCSFramework.util.validation.WURCSValidator;
import org.glycoinfo.WURCSFramework.util.validation.WURCSValidationReport;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;
import org.glycoinfo.WURCSFramework.util.WURCSException;
import org.glycoinfo.WURCSFramework.util.WURCSFactory;
import org.glycoinfo.WURCSFramework.util.graph.WURCSGraphNormalizer;
import org.glycoinfo.WURCSFramework.wurcs.graph.WURCSGraph;
import org.glycoinfo.subsumption.util.graph.analysis.SubsumptionLevel;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.Map;

public class subsumption {
	public static final Map<String, String> propatyMap = new HashMap<String, String>() {
		{
			put("LV2", "Glycosidic_topology");
			put("LV1", "Linkage_defined_saccharide");
			put("LV3", "Monosaccharide_composition_with_linkage");
			put("LV4A", "Monosaccharide_composition");
			put("LV4B", "Base_composition_with_linkage");
			put("LV5", "Base_composition");
		}
	};
	public static final Map<String, String> predicateMap = new HashMap<String, String>() {
		{
			put("LV1toLV2", "has_topology");
			put("LV2toLV3", "has_composition_with_linkage");
			put("LV3toLV4A", "has_base_composition");
			put("LV3toLV4B", "has_base_composition");
			put("LV4AtoLV5", "has_composition");
			put("LV4BtoLV5", "has_composition");
		}
	};
	public static List<HashMap<String, String>> apiMap = new ArrayList<HashMap<String, String>>();

	public static String retJson(String WURCSseq) throws IOException {

		WURCSseq = doValidation(WURCSseq);
		if(WURCSseq == null ){
			System.err.println("{\"message\": \"This wurcs String is not validated.\"}");
			return "";
		}

		try {
			ArrayList<String> items = new ArrayList<String>();
			items.add(WURCSseq);

			while (!items.isEmpty()) {
				for (String item : items) {
					items = converterInterface(item);
				}
			}	
		} catch (SubsumptionException e) {
			//e.printStackTrace();
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(apiMap);
		return json;

	}


	public static String retN3(String WURCSseq) throws IOException {
		WURCSseq = doValidation(WURCSseq);
		if(WURCSseq == null ){
			return "";
		}

		try {
			ArrayList<String> items = new ArrayList<String>();
			items.add(WURCSseq);

			while (!items.isEmpty()) {
				for (String item : items) {
					items = converterInterface(item);
				}
			}	
		} catch (SubsumptionException e) {
			//e.printStackTrace();
		}
		
		String n3 = "";

		for (HashMap<String, String> map : apiMap) {
			String subject = map.get("S");
			//System.out.println(subject);
			String predicate = map.get("P");
			//System.out.println(predicate);
			String object = map.get("O");
			//System.out.println(object);

			if (predicate.contains("rdf:")){
				predicate = predicate.replace("rdf:", "<http://www.w3.org/1999/02/22-rdf-syntax-ns#").trim() + ">";
				
			}
			if (predicate.contains("sbsmpt:")){
				predicate = predicate.replace("sbsmpt:", "<http://www.glycoinfo.org/glyco/owl/relation#").trim() + ">";
				
			}
			if (object.contains("sbsmpt:")){
				object = object.replace("sbsmpt:", "<http://www.glycoinfo.org/glyco/owl/relation#").trim() + ">";
				
			}
			else if (object.contains("WURCS=")){
				object = "\""+ object + "\"";
			}

			n3 += subject + "\t" + predicate + "\t" + object + " .\n";
			
		}
		
		return n3;

	}



	public static String retSha256(String str) {
		String text = str;
		byte[] cipher_byte;
		try{
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(text.getBytes());
			cipher_byte = md.digest();
			StringBuilder sb = new StringBuilder(2 * cipher_byte.length);
			for(byte b: cipher_byte) {
				sb.append(String.format("%02x", b&0xff) );
			}
			text = sb.toString();
		} catch (Exception e) { 
			//e.printStackTrace();
		}
		return text;
	}

	public static ArrayList<String> converterInterface (String _input) throws SubsumptionException {
		ArrayList<String> ret = new ArrayList<String>();
		SubsumptionLevel subLevel = checkSubsumptionLevel(_input);

		if (subLevel.equals(SubsumptionLevel.LV5) || subLevel.equals(SubsumptionLevel.LVX)) {
			return ret;
		}
		if (subLevel.equals(SubsumptionLevel.LV3)) {
			SubsumptionConverter subConv = convertSubsumption(_input);
			ret.add(subConv.getAmbiguousWURCSseq());
			ret.add(subConv.getSecondAmbiguousWURCSSeq());
			return ret;
		}

		ret.add(convertSubsumption(_input).getAmbiguousWURCSseq());
		return ret;
	}
	public static String  doValidation(String str){ 
		String WURCSseq = str;

		WURCSValidator validator = new WURCSValidator();
		validator.start(WURCSseq);

		WURCSValidationReport str_report = validator.getReport();

		if(str_report.equals("")){
			return  str;
		}
		return str;
	}

	public static HashMap<String,String> retTriple(String s, String p ,String o){
		HashMap<String,String> map = new HashMap<String,String>();
		map.put("S",s);
		map.put("P",p);
		map.put("O",o);
		return map;
	}

	public static String retPredicate(String in){
		if (predicateMap.get(in) != null){
			return predicateMap.get(in);
		}
		return "subsumed_by";
	}

	public static SubsumptionConverter convertSubsumption (String _input) throws SubsumptionException {
		SubsumptionConverter sc = new SubsumptionConverter();
		sc.setWURCSseq(_input);
		sc.convertDefined2Ambiguous();
		// debug print
		SubsumptionLevel inputSub = checkSubsumptionLevel(_input);
		SubsumptionLevel outSub = checkSubsumptionLevel(sc.getAmbiguousWURCSseq());

		if(sc.getWURCSseq().equals(sc.getAmbiguousWURCSseq())){
			return new SubsumptionConverter();
		}


		apiMap.add( retTriple( "<http://rdf.glycoinfo.org/glycan/" + retSha256(sc.getWURCSseq()) + ">" ,
					"rdf:type" ,
					"sbsmpt:" + propatyMap.get(inputSub + "")));


		apiMap.add(retTriple("<http://rdf.glycoinfo.org/glycan/" + retSha256(sc.getWURCSseq()) + ">" ,
					"rdf:label"  ,
					sc.getWURCSseq()));

		apiMap.add(retTriple("<http://rdf.glycoinfo.org/glycan/" + retSha256(sc.getWURCSseq()) + ">" ,
					" sbsmpt:" + retPredicate(inputSub + "to" + outSub) ,
					"<http://rdf.glycoinfo.org/glycan/" + retSha256(sc.getAmbiguousWURCSseq()) + ">"));

		apiMap.add(retTriple("<http://rdf.glycoinfo.org/glycan/" + retSha256(sc.getAmbiguousWURCSseq()) + ">" ,
					" rdf:type " ,
					"sbsmpt:" + propatyMap.get(outSub + "")));

		apiMap.add(retTriple("<http://rdf.glycoinfo.org/glycan/" + retSha256(sc.getAmbiguousWURCSseq()) + ">" ,
					"rdf:label" ,
					sc.getAmbiguousWURCSseq() ));

		apiMap.add(retTriple("<http://rdf.glycoinfo.org/glycan/" + retSha256(sc.getAmbiguousWURCSseq()) + ">" ,
					" sbsmpt:subsumes " ,
					"<http://rdf.glycoinfo.org/glycan/" + retSha256(sc.getWURCSseq()) + ">"));


		if (sc.getSecondAmbiguousWURCSSeq() != null) {



			apiMap.add(retTriple("<http://rdf.glycoinfo.org/glycan/" + retSha256(sc.getWURCSseq()) + ">" ,
						"rdf:type" ,
						"sbsmpt:" + propatyMap.get(inputSub + "") ));
			apiMap.add(retTriple("<http://rdf.glycoinfo.org/glycan/" + retSha256(sc.getWURCSseq()) + ">" ,
						"rdf:label" ,
						sc.getWURCSseq() ));
			apiMap.add(retTriple("<http://rdf.glycoinfo.org/glycan/" + retSha256(sc.getWURCSseq()) + ">" ,
						" sbsmpt:" + retPredicate(inputSub + "to" + checkSubsumptionLevel(sc.getSecondAmbiguousWURCSSeq())) ,
						" <http://rdf.glycoinfo.org/glycan/" + retSha256(sc.getSecondAmbiguousWURCSSeq()) + "> "));

			apiMap.add(retTriple("<http://rdf.glycoinfo.org/glycan/" + retSha256(sc.getSecondAmbiguousWURCSSeq()) + ">" ,
						"rdf:type" ,
						"sbsmpt:" + propatyMap.get(checkSubsumptionLevel(sc.getSecondAmbiguousWURCSSeq()) + "") ));
			apiMap.add(retTriple("<http://rdf.glycoinfo.org/glycan/" + retSha256(sc.getSecondAmbiguousWURCSSeq()) + ">" ,
						" rdf:label" ,
						sc.getSecondAmbiguousWURCSSeq()));
			apiMap.add(retTriple("<http://rdf.glycoinfo.org/glycan/" + retSha256(sc.getSecondAmbiguousWURCSSeq()) + ">",
						" sbsmpt:subsumes " ,
						"<http://rdf.glycoinfo.org/glycan/" + retSha256(sc.getWURCSseq()) + ">"));

		}
		return sc;
	}

	public static SubsumptionLevel checkSubsumptionLevel (String _input) {
		SubsumptionLevel subLevel = SubsumptionLevel.LVX;
		try {
			WURCSFactory wf = new WURCSFactory(_input);
			WURCSGraph graph = wf.getGraph();
			WURCSGraphNormalizer wgNorm = new WURCSGraphNormalizer();
			wgNorm.start(graph);

			WURCSGraphStateDeterminator wgsd = new WURCSGraphStateDeterminator();
			subLevel = wgsd.getSubsumptionLevel(graph);
		} catch (WURCSException e) {
			//e.printStackTrace();
		}
		return subLevel;
	}
}
